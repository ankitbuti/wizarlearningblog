<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
   <title>Early Learning, Parenting and Education | Wizar Learning</title>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">    
	<?php wp_head(); ?>
<script>(function(d, s, id) { 
var js, fjs = d.getElementsByTagName(s)[0]; 
if (d.getElementById(id)) return; 
js = d.createElement(s); js.id = id; 
js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.11'; 
fjs.parentNode.insertBefore(js, fjs); 
}(document, 'script', 'facebook-jssdk'));</script>

<meta name="google-site-verification" content="QDxHPDkw652T1MaZB42hl2fkRv8YnwExb83tcrqnsQI" /></head>
<body  <?php body_class(); ?>>

    <header id="header"  class="header fixed">
        <div class="header-wrap container">
            <div class="theta-logo">
				<?php if ( has_custom_logo() ){?>
                <div class="theta-logo-img first-logo">	
					<a href="<?php echo esc_url('http://www.wizarkids.com'); ?>" target="_blank" class="redirect_url"> <?php the_custom_logo(); ?></a>
                </div>
				<div class="theta-logo-img second-logo">	
                    <a href="http://www.wizar.io" class="custom-logo-link" rel="wizario" itemprop="url"><img width="189" height="70" src="http://blog.wizarlearning.com/wp-content/uploads/2018/01/wizario_logo.png" class="custom-logo" alt="Wizario" itemprop="logo" srcset="http://blog.wizarlearning.com/wp-content/uploads/2018/01/wizario_logo.png 224w, http://blog.wizarlearning.com/wp-content/uploads/2018/01/wizario_logo.png 36w, http://blog.wizarlearning.com/wp-content/uploads/2018/01/wizario_logo.png 48w" sizes="(max-width: 224px) 100vw, 224px"></a>                </div>
               <?php }?> 

                <div class="theta-logo-text">
                    <a href="<?php echo esc_url(home_url('/')); ?>"><span class="blog-name"><?php bloginfo('name'); ?></span></a><br>
                    <a href="<?php echo esc_url(home_url('/')); ?>"><span class="blog-description"><?php bloginfo('description'); ?></span></a>
                </div>
            </div>

            <nav id="theta-menu" class="theta-menu">
                <div class="menu-icon"><i class="icon-menu"></i><i class="icon-close"></i></div>
                <!-- Mobile button -->
                
                <ul class="menu">
                <li id="search-site"><a class="theta-search" href="javascript:;"><i class="icon-search"></i></a></li>
           <span>  
                <?php
				
					if ( has_nav_menu( 'header-menu' ) ) {
                    	 wp_nav_menu( array( 'theme_location' => 'header-menu', 'items_wrap' => '%3$s','container' => false  ) );
                  	}
				?>
              </span>    
                </ul>
                
                <ul class="menu-mobile">
                </ul> 
                
                
                <div id="theta-top-search">
                	<span class="theta-close-search-field"></span>
                <?php 
                        get_search_form();
                ?>
                
                </div>                
                    
            </nav>
            
 
        </div><!--div class="header-wrap"-->
	<!--	<div class="cat-container">
 <?php echo do_shortcode('[searchandfilter headings="Select categories:" types="checkbox" fields="category"]'); ?>
		</div> -->
	</header>