<?php 
//post page

get_header(); ?> 
    
   <div class="warp">
        <div class="main-content">
			
  			<section class="theta-article-content container">

            <?php  theta_breadcrumb_trail();//echo theta_breadcrumbs(); ?> 
  				<div class="single-content">
				<?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>
                    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                        <h2><?php the_title(); ?></h2>
                        <div class="author-share">
                            <div class="author">
                            	<?php echo get_avatar( the_author_meta('user_nicename'), 48,'','',false ); ?>
                                
                                <p><time><?php the_date();?></time></p>
                            </div>

                            
                        </div>
                        <div class="blog-article-content">
                            <?php the_content(); ?>
							<div class="navigation">
								<div class="pull-left"><?php next_post(' %', '&laquo; Previous Post', 'no'); ?>
								 </div>								
								<div class="pull-right"><?php previous_post('% ', 'Next Post &raquo; ', 'no'); ?>
								 </div>
							</div> 

						<?php
                        
                        
                            $defaults = array(
                                'before'           => '<p class="post-paged">' . __( 'Pages:','theta' ),
                                'after'            => '</p>',
                                'link_before'      => '',
                                'link_after'       => '',
                                'next_or_number'   => 'number',
                                'separator'        => ' ',
                                'nextpagelink'     => __( 'Next page','theta' ),
                                'previouspagelink' => __( 'Previous page','theta' ),
                                'pagelink'         => '%',
                                'echo'             => 1
                            );
                         
                            wp_link_pages( $defaults );
                    
                        ?>
			
                        </div>
                    </div>
                    

                    
                    <?php endwhile;endif; ?> 
					<?php if(has_tag()){?>
                        <div id="article-tag">
                            <?php the_tags('<strong>'.__( 'Tags:','theta' ).'</strong> ', ''._x( ' , ', 'tags separator', 'theta' ).'' , ''); ?>
                        </div> 
                    <?php }?> 
                    
                    
                                    
                
					<?php
                        if(comments_open()){
							?>
					<div id="fb-root"></div> 
				 <div class="fb-comments" data-href="<?php the_permalink(); ?>" data-num-posts="5" data-width="100%" data-colorscheme="light" data-mobile="false"></div>  
					<?php
						comments_template();
						}
                    ?>
              	</div>
              
              
            	<?php get_sidebar(); ?>
                

        	</section>      
        

		</div>
    </div>
 
<?php get_footer(); ?>