 
  	<footer id="footer" class="footer">
    
               
    	<div class="footer-bottom container">
      		<p class="copyright"><span class="pull-right mobile-t-c" onclick="window.open('http://blog.wizarlearning.com/terms/')">TERMS &amp; CONDITIONS</span><?php printf(__('Copyright © 2017 <a href="http://wizarlearning.com/">WizAR Learning Solutions Private Limited.</a> All Rights Reserved.&nbsp;&nbsp;','All Rights Reserved'),esc_url('http://wizarlearning.com/'),esc_url('http://wizarlearning.com/'));?><span class="desktop-t-c" onclick="window.open('http://blog.wizarlearning.com/terms/')">TERMS &amp; CONDITIONS</span></p>
			 <div class="pull-right socialbar">  
				 <div class="socialIcons" style="border-bottom: 1px solid #ddd;">
					   <div class="socialicon"><a href="https://www.facebook.com/WizarioLMS/" target="_blank" title="Wizario-FB Page"><img src="http://blog.wizarlearning.com/wp-content/uploads/2017/12/fb.jpg" alt="Wizario-FB"></a></div>
					   <div class="socialicon"><a href="https://www.facebook.com/wizarkids/" target="_blank" title="WizarKids-FB Page"><img src="http://blog.wizarlearning.com/wp-content/uploads/2017/12/fb.jpg" alt="WizarKids-FB"></a></div>
					 <div class="socialicon"><a href="https://www.linkedin.com/in/wizarlearning/" target="_blank" title="Linkedin"><img src="http://blog.wizarlearning.com/wp-content/uploads/2017/12/linkedin-1.png" alt="Linkedin"></a></div>
				 <div class="socialicon"><a href="https://twitter.com/WizarLearning" target="_blank" title="Twitter"><img src="http://blog.wizarlearning.com/wp-content/uploads/2017/12/twitter-1.png" alt="Twitter"></a></div>
                        </div>
			</div>
        </div>
    </footer>
    <?php wp_footer(); ?> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110792851-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

 gtag('config', 'UA-110792851-3');
</script>
<script>
$(document).ready(function () {
$("#contactus_submit").on("click", function(e){
  e.preventDefault();

console.log("con");
var data = new Object();	
data.UserType = "b";
data.FullName = $("#contactus_name").val();
data.Email = $("#contactus_email").val();
data.Message = $("#contactus_msg").val();
console.log(data);
	if(data.FullName=="" || data.Email=="" || data.Message=="")
		{
			alert('Please fill all field');
		}
	else{
$.ajax({
			//send contact to web api for forwarding email and storing in db
			type: "POST",
			url: "http://dev.wizar.io/user/contact",
			data: JSON.stringify(data),
			contentType: "application/json; charset=utf-8",
			success: function (response) {
                alert("Message sent successfully! ");
			},
			error: function (response) {
                 alert("Message not sent");
            }
		});
	}

});
});
</script>
</body>
</html>      
   