<?php 
//post page

get_header(); ?> 
 
    
   <div class="warp">
        <div class="main-content">
			
  			<section class="theta-article-content container">
            <?php theta_breadcrumb_trail(); ?> 
  				<div class="single-content">
                    		<h3><?php esc_html_e('404 Page!','theta');?></h3>
                    		<div class="quote">
                            	<p><?php  esc_html_e('404 not found!','theta')?> <a href="<?php echo esc_url(home_url('/'));?>"><i class="fa fa-home"></i> <?php  esc_html_e('Please, return to homepage!','theta')?></a></p>
							</div>
              	</div>
              
              
            	<?php get_sidebar(); ?>
                            
        	</section>      
        

		</div>
    </div>
 
<?php get_footer(); ?>