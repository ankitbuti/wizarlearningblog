 
  	<footer id="footer" class="footer">
    
               
    	<div class="footer-bottom container">
      		<p class="copyright"><?php printf(__('Copyright © 2017 <a href="http://wizarlearning.com/">WizAR Learning Solutions Private Limited. All Rights Reserved</a>.','All Rights Reserved'),esc_url('http://wizarlearning.com/'),esc_url('http://wizarlearning.com/'));?></p>
			<ul class="pull-right social-ico-style">
                            <li><a href="https://www.facebook.com/WizarioLMS/" target="_blank" title="Facebook"><img src="http://localhost:4567/wordpress/wp-content/uploads/2017/12/facebook.png" alt="Facebook"></a></li>
                            <li><a href="https://www.linkedin.com/in/wizarlearning/" target="_blank" title="Linkedin"><img src="http://localhost:4567/wordpress/wp-content/uploads/2017/12/linkedin.png" alt="Linkedin"></a></li>
                            <li><a href="https://twitter.com/WizarLearning" target="_blank" title="Twitter"><img src="http://localhost:4567/wordpress/wp-content/uploads/2017/12/twitter.png" alt="Twitter"></a></li>
                        </ul>
        </div>
    </footer>
    <?php wp_footer(); ?>    
</body>
</html>      
   