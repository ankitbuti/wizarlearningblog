<?php

function theta_setup(){
	global $content_width;

	$lang = get_template_directory(). '/languages';
	load_theme_textdomain('theta', $lang);
	require get_template_directory() . '/inc/breadcrumb-trail.php';
    require_once( trailingslashit( get_template_directory() ) . 'inc/customizer-pro/class-customize.php' );

	if ( function_exists( 'add_image_size' ) ) add_theme_support( 'post-thumbnails' );
	if ( function_exists( 'add_image_size' ) ) { 
		add_image_size( 'theta-thumb', 360, 186 );
	}

	$args = array();
	$header_args = array( 
	    'default-image'          => '',
		'default-repeat' => 'no-repeat',
        'default-text-color'     => '00BAE1',
		'url'                    => '',
        'width'                  => 1920,
        'height'                 => 89,
        'flex-height'            => true
     );
	$background_args = array(
		'default-color' => 'f7f8f8',
	);	 
		 
	add_theme_support( 'custom-background', $background_args );
	add_theme_support( 'custom-header', $header_args );
	add_theme_support( 'automatic-feed-links' );//

	add_theme_support( 'custom-logo', array(
			   'height'      => '70',
			   'width'       => '224',
			   'flex-width' => true,
			) );

	//register menus
	register_nav_menus(
					   array(
						'header-menu' => __( 'Header Menu', 'theta' ) ,
					   	'footer-menu' => __( 'Footer Menu', 'theta' )	
					   )
					   );					   
					   
	
	add_theme_support( "title-tag" );//
	add_editor_style("css/editor-style.css");
	if ( !isset( $content_width ) ) $content_width = 1170;	
}
add_action( 'after_setup_theme', 'theta_setup' );

	
function theta_custom_scripts()
{
	$theme_info = wp_get_theme();
	
	wp_enqueue_style('theta-base',  get_template_directory_uri() .'/css/base.css', false, $theme_info->get( 'Version' ), false);
	wp_enqueue_style('font-awesome',  get_template_directory_uri() .'/css/font-awesome.min.css', false,"4.6.3", false);	
	wp_enqueue_style('bootstrap',  get_template_directory_uri() .'/css/bootstrap.min.css', false,"3.3.7", false);	
		

	wp_enqueue_style('theta-style', get_stylesheet_uri(), false, $theme_info->get( 'Version' ) );	
	
	
	
	wp_enqueue_style(
		'theta-custom-style',
		get_template_directory_uri() . '/css/custom_script.css'
	);
	$custom_css = '';
	if ( has_custom_logo() ){
		$custom_css .= "@media screen and (max-width:1025px){
			.theta-logo-text{ display:none;}
			.theta-logo{ margin-left:20px;}	
			
		}";
	 }else{
		$custom_css .= "@media screen and (max-width:1025px){
			.blog-description{ display:none;}
			.theta-logo-text{ margin-top:10px;}	
		}";
		
	}
	
	
	if ( get_header_textcolor() !='' && get_header_textcolor()!= '2C2C2C' ){
	
		$custom_css .='.theta-logo-text .blog-name,.theta-logo-text .blog-description,.theta-menu li>a,.theta-menu ul.menu li.menu-item-has-children ul li.menu-item-has-children a:after, .theta-menu ul.menu li.menu-item-has-children a:after,.theta-search a.search-icon{color:#'.esc_attr(get_header_textcolor()).';}';	

	}
		
	if ( get_background_color() !=''  ){
		$custom_css .='html body{background-image:none;}';	
	}	
	$header_image = get_header_image();	
	//echo '-----888-------';		
	if ( ! empty( $header_image ) ){

		$custom_css .='header {
			background-image: url('.esc_url($header_image).');	background-repeat: no-repeat;background-position: 50% 50%;	-webkit-background-size: cover;	-moz-background-size:  cover;
			-o-background-size:      cover;	background-size:         cover;
		}
		.main-nav,.nav-wrap ul.menu li.menu-item-has-children { background-color:transparent;}
		@media screen and (min-width: 59.6875em) {
			body:before {
				background-image: url('.esc_url($header_image).');	background-repeat: no-repeat;background-position: 100% 50%;	-webkit-background-size: cover;	-moz-background-size: cover;
				-o-background-size:  cover;	background-size:   cover;		border-right: 0;
			}
		}';	

	}		
	
    wp_add_inline_style( 'theta-custom-style', $custom_css );
		
		

	wp_enqueue_script('bootstrap', get_template_directory_uri().'/js/bootstrap.min.js', array( 'jquery' ), '3.3.7', false );				
	wp_enqueue_script('theta-main', get_template_directory_uri().'/js/main.js', array( 'jquery' ),$theme_info->get( 'Version' ), false );				
}

add_action( 'wp_enqueue_scripts', 'theta_custom_scripts' );





function theta_better_comments($comment, $args, $depth)
{

?>	
 
    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
        <article id="comment-<?php comment_ID(); ?>" class="comment-body">
            <div class="comment_avatar">
                <?php echo get_avatar($comment, $size = '45'); ?>
            </div>

            <div class="comment_postinfo">
                <?php printf(__('<cite class="fn">%s</cite> <span class="says"> on </span>','theta'), get_comment_author_link()) ?>
                <span class="comment_date"><?php printf(__('%1$s at %2$s','theta'), get_comment_date(),  get_comment_time()) ?></span><?php edit_comment_link(__('(Edit)','theta'),'  ','') ?>
            </div> <!-- .comment_postinfo -->

            <div class="comment_area">				
                <div class="comment-content clearfix">
                	<?php if ($comment->comment_approved == '0') : ?>
                     	<em><?php esc_html_e('Your comment is awaiting moderation.','theta') ?></em>
                     	<br />
                    <?php endif; ?>
                    
                    <?php comment_text() ?>
                    
                    
                    <div class="reply-container">
                        <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                    </div>
                      
                </div> <!-- end comment-content-->
            </div> <!-- end comment_area-->
        </article> <!-- .comment-body -->
        
<?php
}


/*
add widgets to wp-admin
*/
function theta_widgets_init() {

	register_sidebar( array(
		'name' => __('Sidebar','theta'),
		'id' => 'sidebar',
		'before_widget' => '<div class="sidebar-section"><ul class="blog-category">
',
		'after_widget' => '</ul></div> <!-- end .sidebar-section -->',
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	) );


}
add_action( 'widgets_init', 'theta_widgets_init' );



/* this function gets thumbnail from Post Thumbnail or Custom field or First post image */
if ( ! function_exists( 'theta_get_thumbnail' ) ) {
	function theta_get_thumbnail($post_id)
	{
		if(has_post_thumbnail())
		{
			
			$ct_post_thumbnail_fullpath=wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), "Full");
			$thumb_array['fullpath'] = $ct_post_thumbnail_fullpath[0];
		
		}else{
			$post_content = get_post($post_id)->post_content;
			$thumb_array['fullpath'] = theta_catch_that_image($post_content);
		}	
		
		if($thumb_array['fullpath']=="" )
		{
			
			$thumb_array['fullpath'] = get_template_directory_uri()."/images/default.jpg";
		
		}		

		return $thumb_array;
		
	}
}

function theta_catch_that_image($post_content)
{
  global $post, $posts;
  $first_img = '';
  ob_start();
  ob_end_clean();
  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post_content, $matches);
  if($output!='')  $first_img = $matches[1][0];

  return $first_img;
}